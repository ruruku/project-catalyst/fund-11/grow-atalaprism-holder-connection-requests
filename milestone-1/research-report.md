# Workshop research report: Holder Connection Requests

*Date: May 9, 2024*

**Introduction**

The workshop on Holder-Initiated Connections held on May 9, 2024, convened with the aim of exploring the utilization of the SDK within the community and addressing any requirements regarding Holder-initiated Connections. The session provided a platform for attendees to share insights, experiences, and queries related to this topic.

**Attendees and Context**

The workshop commenced with introductions from the participants, offering a diverse mix of backgrounds and geographical locations:

1. **Lawrence** from Toronto, a teacher and blockchain consultant, currently exploring credentials experimentation.
2. **Seira** from Japan, representing [Socious.io](http://socious.io/), a talent marketplace connecting organizations and contractors, particularly focusing on impact-driven entities.
3. Mix Irving from Aotearoa, NZ, Tech lead for [Ahau.io](http://Ahau.io) a distributed platform for tribal communities leveraging AtalaPrism SDK for tribal membership identity credentials.

**Key Discussions and Insights**

**1. Holder-Initiated Connections Use Cases:**

- Lawrence highlighted the importance of enabling holders to initiate connections without waiting for requests from organizations. This feature was viewed as crucial for enhancing user autonomy and streamlining processes.
- Seira provided a use case scenario involving potential employers and job seekers, emphasizing the preference for direct interaction rather than mediated verification through platforms like Socious.

**2. Technical Considerations:**

- Discussions delved into technical aspects, including the verification of credentials initiated by holders. Lawrence emphasized the need for trust registries and mechanisms to verify the authenticity of issuers, suggesting the use of public keys for verification.
- The concept of mediators was explored, with considerations on decentralization and the role of wallets in initiating processes. Lawrence proposed the use of multisig and hardware wallets for secure issuance.

**3. Infrastructure and Issuance:**

- The conversation expanded to infrastructure requirements for running an issuer node. Lawrence mentioned platforms like Koios.rest for community-driven service infrastructures and highlighted the importance of threshold signing and multisig for secure issuance.

**4. Collaboration and Future Directions:**

- Attendees discussed the potential for collaboration and further exploration of SDK capabilities, particularly in enhancing user-centric features and decentralized processes.
- Lawrence emphasized the need for continued research and development in enabling secure and user-friendly Holder-initiated Connections.

**Recommendations:**

Based on the discussions and insights shared during the workshop, several recommendations emerge to enhance the implementation and utilization of Holder-initiated Connections:

1. **User-Centric Design:** Developers should prioritize user autonomy and convenience in the design of SDKs, enabling holders to initiate connections seamlessly.
2. **Enhanced Verification Mechanisms:** Robust verification mechanisms should be implemented to ensure the authenticity of credentials initiated by holders, including the development of trust registries and decentralized verification protocols.
3. **Secure Infrastructure:** Organizations involved in running issuer nodes should prioritize security measures such as multisig and hardware wallets to safeguard against potential vulnerabilities.
4. **Community Collaboration:** Collaboration among stakeholders is crucial for driving innovation and addressing challenges in implementing Holder-initiated Connections. Open dialogue and collaborative research efforts can accelerate progress in this domain.
5. **Continued Research and Development:** Ongoing research and development are essential for advancing the capabilities of SDKs and enhancing user autonomy in credential verification processes.

By implementing these recommendations, stakeholders can contribute to the advancement of Holder-initiated Connections, empowering users with greater control over their digital identities while ensuring security and trust in credential verification processes.

**Conclusion**

The workshop provided valuable insights into the utilization of Holder-initiated Connections within the community, addressing both technical considerations and practical use cases. Collaboration and ongoing research efforts were highlighted as essential for advancing the capabilities of SDKs and enhancing user autonomy in credential verification processes.

**References:**

- Catalyst Grant
- Ahau SDK Use
- [Socious.io](http://socious.io/)
- Koios.rest

*Prepared for Project Catalyst Project [Grow AtalaPrism: Holder connection requests](https://projectcatalyst.io/funds/11/cardano-open-developers/grow-atalaprism-holder-connection-requests)  by: Ben Tairea*
