# Product Requirements

## Introduction

The Atala Prism Wallet SDK is a tool that helps people manage their digital
identities and credentials securely. It's like a digital wallet for your online
identity. As technology evolves, we need to keep up with new ways to make our
digital lives easier and safer. That's why we're working on adding a new feature
to the Atala Prism Wallet SDK: the ability for holders to initate connections
with cloud agents. This upgrade will open up exciting possibilities for how
people use their online credentials.

## Objective

Our main goal is to extend the `@atala/prism-wallet-sdk` module to let holders
initiate connections with cloud agents. Currently, holders have to be sent a
connection invitation by a third party (inviter). This can be slow for use cases
online, because holders have to ask for a connection invitation, then accept it.

We want to make it easier for holders to be issued and/or have their credentials
verified by removing a step in the connection process and allowing holders to
proactively initiate connections with cloud agents. Cloud agents will be
configurable to automatically accept these connection invitations. This will
speed up establishing connections between holders and cloud agents, without the
need for a third party to start the connection process.

## System Architecture

See [System Architecture](./architecture.md)

## AtalaPrism - User Stories

1. As a **holder**, I can initiate connections with **cloud agents** using just their DID
2. As a **cloud agent**, I want to receive connection invitations directly from holders and automatically accept them, so that I can streamline the process of establishing connections and providing credential issuance or verification services
3. As a **cloud agent admin,** I want to be able to configure my **cloud agent** to accept connections initiated by others.


**Stretch goal:**

4. As a **holder**, I want to have visibility into the status of my connection requests with **cloud agents** (issuers and verifiers) when using the Atala Prism Wallet SDK, to track the progress of my interactions.
    - The SDK provides methods or functions for users to query the status of their connection requests with issuer and verifier agents.
    - Users receive real-time updates on the status of their connection requests, including pending, accepted, or rejected statuses.
    - The SDK offers error messages or notifications in case of failed connection attempts or other issues encountered during the process.

### Current connection process

```mermaid
sequenceDiagram

actor Invitee
participant Issuer as Cloud Agent
actor Inviter

autonumber

Inviter ->> Issuer: send a connection request
Issuer ->> Issuer: generate a connection invitation
Issuer ->> Inviter: send connection invitation
Inviter ->> Invitee: send connection invitation
Invitee ->> Inviter: accept connection invitation
```

### Improved connection process

```mermaid
sequenceDiagram

actor Invitee
participant Issuer as Cloud Agent
actor Inviter

autonumber

Inviter ->> Invitee: broadcast cloud agent DID
Invitee ->> Invitee: generate a connection request
Invitee ->> Issuer: send a connection request
Issuer ->> Invitee: accept connection request
```

## Other User Stories / Use Caess

### Real World

- As a customer to a supermarket, I want to proactively offer my age credentials if I'm buying a bottle of wine. (I don't want to wait for a slow process initiated by the check-out staff).

### Āhau

- In Āhau, we want to reduce the amount of steps needed in the tribal registration process (adding new members to a tribe), and make it easier for Tribal Members (holders) to connect to cloud agents, without the need for Tribal Kaitiaki (admin) to initiate it.
- If we assume any tribe that is set up to issue is also set up to verify:

    1. As a potential **Tribal Member**, when I register for a tribe **that issues and/or verifies credentials**

        a) When I complete the tribal registration form, I proactively start the connection process:
        - I send my registration to the **Tribal Kaitiaki**
        ii. I look for the **Issuer** or **Verifier DID** on the Tribes information
        - I generate a connection request message
        - I send the connection request message to the **Cloud Agent**

    2. As a **Tribal** **Kaitiaki** that **issues and/or verifies credentials**

        a) When I receive a tribal registration

        - I can see that the **potential Tribal Member** is connected to the **Cloud Agent**

        b) accept the tribal registration request
        - It either goes:
            1. The current issue credential path and/or
            2. The current presentation path

- **Current registration flow:**

    ```mermaid
    sequenceDiagram
    actor Invitee as Tribal Member
    participant Issuer as Cloud Agent
    actor Inviter as Tribal Kaitiaki
    
    autonumber

    note over Invitee,Inviter: registration process
    Invitee ->> Inviter: send tribal registration
    Inviter ->> Invitee: accept registration
    
    note over Invitee,Inviter: connection
    Inviter ->> Issuer: request connection invitation
    Issuer ->> Inviter: send connection invitation
    Inviter ->> Invitee: send connection invitation
    Invitee ->> Issuer: accept connection invitation
    ```

- **Improved registration process:**

    ```mermaid
    sequenceDiagram
    
    actor Invitee as Tribal Member
    participant Issuer as Cloud Agent
    actor Inviter as Tribal Kaitiaki
    
    autonumber

    Inviter ->> Invitee: broadcast cloud agent DID
    
    Invitee ->> Invitee: formulate registration and connection request
    Invitee ->> Inviter: send tribal registration
    Invitee ->> Issuer: send connection request
    
    Issuer ->> Invitee: accept connection request
    Inviter ->> Issuer: is this Tribal Member connected?
    Issuer ->> Inviter: yes!
    Inviter ->> Invitee: accept registration and offer credentials
    Invitee ->> Inviter: accept credentials
    ```

- More notes on the Āhau Tribal Registration Process [here](./ahau-registration-process.md)

## **Functional Requirements**

- Atala Prism Wallet SDK Enhancement:
  - Develop the functionality for the Prism Wallet Agent to initiate connection requests with cloud agents.
- Atala Prism Cloud Agent Upgrades:
  - Auto-Accept Connections:
    - Upgrade the cloud agents to automatically accept incoming connection requests from Wallet SDK agents.
- Use Case Demonstration:
  - Āhau Integration:
    - Update the `ssb-atala-prism` module to leverage enhanced wallet and cloud agent features.
    - Update the Tribal registration process:
      - Auto connection: When a potential Tribal Member submits a registration request to join a Tribe (that issues or verifies credentials), this generates a connection request which gets sent to the cloud agent for connection.
    - Update the Tribal registration review process:
      - Auto presentation: When a Tribal Kaitiaki receives a registration that includes a presentation, the connection is accepted and the verification process is started.
      - Auto issuance: When a Tribal Kaitiaki reviews a registration and approves it, they can see the if the potential tribal member is connected to the cloud agent (or not).
- Documentation:
  - Make updates to the documentation in relevant repos to include the changes
- Testing and Quality Assurance:
  - Update the testing suite in relevant repos to include feature changes to identify and address any issues.
- Merge Pull Requests:
  - Communicate with Atala Prism core team to have contributions merged and made available to all prism developers

## **Non Functional Requirements**

1. **Performance:**
    - **Response Time:** The Atala Prism Wallet SDK Agents and Cloud Agents should maintain low response times when initiating connections and handling presentation requests to ensure a smooth user experience.
    - **Throughput:** The system should maintain the capability of handling a large number of connection requests and presentation requests simultaneously without significant degradation in performance.
2. **Security:**
    - **Authentication:** Users should be authenticated securely when initiating connection requests or responding to presentation requests to prevent unauthorised access to sensitive information.
    - **Authorisation:** Cloud Agents should verify the authorisation of Atala Prism Wallet SDK Agents before accepting connection requests or responding to presentation requests to ensure data privacy and integrity.
3. **Reliability:**
    - **Availability:** The system should maintain high availability to ensure that users can initiate connections and access presentation requests without downtime.

## Risks

1. **Community Engagement Risk:**
    - Limited community participation may overlook important requirements.
    - Mitigation: Proactively engage and encourage community involvement.
2. **Technical Complexity Risk:**
    - Unforeseen technical challenges may delay development.
    - Mitigation: Conduct thorough research, break tasks into manageable chunks, seek advice from the Atala Prism team.
3. **Integration Risk:**
    - Integrating new functionalities may cause compatibility issues.
    - Mitigation: Conduct comprehensive compatibility testing, collaborate with the Atala Prism team.
4. **Timeline Risk:**
    - Delays in development or stakeholder approvals may extend the timeline.
    - Mitigation: Develop a realistic timeline, monitor progress, and adjust schedules.
5. **Resource Constraints Risk:**
    - Limited resources may impede project progress.
    - Mitigation: Conduct resource assessment, prioritise tasks, seek additional resources if needed.
6. **Quality Assurance Risk:**
    - Inadequate testing may result in unreliable software.
    - Mitigation: Implement rigorous testing methodologies, automate testing, conduct thorough regression testing.
7. **Documentation Risk:**
    - Incomplete documentation may hinder adoption and troubleshooting.
    - Mitigation: Prioritise comprehensive and updated documentation in multiple formats.
8. **Dependency Risk:**
    - Third-party dependencies may undergo changes or become deprecated.
    - Mitigation: Monitor dependencies, maintain version control, and stay informed about alternatives.

## Testing and Quality Assurance

### Testing Strategy:

1. **Unit Testing:**
    - Develop unit tests to verify the functionality of individual components, such as the Prism Wallet SDK Agents and Cloud Agents.
    - Ensure that unit tests cover all edge cases and scenarios, including successful connection initiation, invitation generation, and automatic acceptance.
2. **Integration Testing:**
    - Perform integration tests to validate the interaction between the Prism Wallet SDK Agents and Cloud Agents.
    - Test scenarios involving multiple components working together, such as initiating connections, handling presentation requests, and verifying credentials.
3. **End-to-End Testing:**
    - Conduct end-to-end tests to simulate real-world user scenarios, from initiating a connection in the wallet to successfully establishing a connection with a cloud agent.
    - Validate the entire workflow, including invitation generation, automatic acceptance, presentation request handling, and credential verification.
4. **Performance Testing:**
    - Measure the response time and throughput of the system under different loads to ensure it meets performance requirements.
    - Stress test the system to determine its scalability and identify any bottlenecks or performance issues.
5. **Security Testing:**
    - Perform security testing to identify and address potential vulnerabilities in the connection initiation process and data transmission between agents.
    - Verify that authentication and authorisation mechanisms are robust and prevent unauthorised access.
6. **Reliability Testing:**
    - Test the availability and fault tolerance of the system by simulating various failure scenarios, such as server crashes or network outages.
    - Verify that the system can recover gracefully from failures without compromising user experience or data integrity.

### Quality Assurance:

1. **Test Coverage:**
    - Ensure comprehensive test coverage by addressing all functional and non-functional requirements outlined in the product requirements document.
    - Track test coverage metrics to identify areas that require additional testing or refinement.
2. **Automated Testing:**
    - Implement automated testing wherever possible to streamline the testing process and ensure consistent results.
    - Use testing frameworks and tools to automate unit tests, integration tests, and regression tests.
3. **Documentation Review:**
    - Review and update documentation to reflect changes in the product, including new features, functionalities, and testing procedures.
    - Ensure that documentation is clear, accurate, and accessible to developers, testers, and end-users.
4. **Code Review:**
    - Conduct code reviews to assess the quality of the implementation and identify any potential issues or areas for improvement.
    - Ensure adherence to coding standards, best practices, and security guidelines.
5. **User Acceptance Testing (UAT):**
    - Involve stakeholders and end-users in UAT to validate the functionality and usability of the new feature.
    - Gather feedback from users to identify any issues or areas for improvement before release.
6. **Regression Testing:**
    - Perform regression testing to ensure that existing functionalities are not affected by the introduction of the new feature.
    - Re-run previously executed tests to verify that no regressions or unintended consequences have occurred.
